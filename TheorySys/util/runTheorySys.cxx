///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2021 CERN for the benefit of the ATLAS collaboration
*/
// runTheorySys.cxx
// application for TheorySys
// run as: runTheorySys samples.txt
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>  
#include "TROOT.h"
#include "TFile.h"
#include <vector>
#include "TheorySys/TheorySys.h"
using namespace std;  

std::map<int,double> Ngen_a;
std::map<int,double> Ngen_d;
std::map<int,double> Ngen_e;
void MapNgen();

int main(int argc, char* argv []) {
	gROOT->SetBatch(kTRUE);
	if(argc == 1) {
		cerr<< "Input file is required" << endl; 
		return 0;
	}
	
	ifstream input(argv[1]);
	if(!input) {
        		cerr << "Cannot open the File : "<<argv[1]<<endl;
        		return 0;
    	}
    	
    	// Get the number of event once;
    	
    	MapNgen();
    	
    	std::string str;
    	
    	while (std::getline(input, str))
    	{
        		if(str.size() <= 0) continue;
        		vector<std::string> samples;
    		vector<std::string> trees;
        		TString sTr(str);
        		
        		if(sTr.BeginsWith("#")) continue;
        		
        		auto token = sTr.Tokenize(" ");
        		for(int i = 0; i<token->GetEntries()-1; i++)
        		{
        			samples.push_back(token->At(i)->GetName());
        		}
        		cout<<str<<endl;
        		//TString path="/eos/user/h/heljarra/NominalSysTrees";
        		//TString path="/eos/home-r/rgarg/Rocky/DarkPhotonMicroNtuples_V17Sys_BDTweights/Var6";
        		TString path="/eos/user/r/rgarg/Rocky/DarkPhotonMicroNtuples_V17Sys_BDTweights_2branches"; // BDT latest with/without AHOI
        		cout<< "RUNNING On: " << token->First()->GetName() << endl;
        		TheorySys* obj = new TheorySys(path, samples, token->Last()->GetName());
        		obj->initilize(Ngen_a, Ngen_d, Ngen_e);
        		obj->execute();
		obj->finilize();
		delete obj;
   	 }
	
	return 1;
}

void MapNgen() {

  TFile *f_a = new TFile(PathResolverFindCalibFile("TheorySys/mc16aNorm.root").c_str(),"READ");
  TFile *f_d = new TFile(PathResolverFindCalibFile("TheorySys/mc16dNorm.root").c_str(),"READ");
  TFile *f_e = new TFile(PathResolverFindCalibFile("TheorySys/mc16eNorm.root").c_str(),"READ");
  
  TH1D* h_a = (TH1D*) f_a->Get("h_total"); h_a->SetName("mc16a");
  TH1D* h_d = (TH1D*) f_d->Get("h_total"); h_d->SetName("mc16d");
  TH1D* h_e = (TH1D*) f_e->Get("h_total"); h_e->SetName("mc16e"); // change the name for memory problems
  
  if(!h_a){ cout<<"Number of events not found"<<endl; return;}

  for(int i=1; i<=h_a->GetNbinsX();i++){
  
	    TString tmp = h_a->GetXaxis()->GetBinLabel(i);
	    int dsid = tmp.Atoi();
	    double N = h_a->GetBinContent(i);
	    Ngen_a[dsid]=N;
   }
   
   for(int i=1; i<=h_d->GetNbinsX();i++){
  
	    TString tmp = h_d->GetXaxis()->GetBinLabel(i);
	    int dsid = tmp.Atoi();
	    double N = h_d->GetBinContent(i);
	    Ngen_d[dsid]=N;
   }
   
   for(int i=1; i<=h_e->GetNbinsX();i++){
  
	    TString tmp = h_e->GetXaxis()->GetBinLabel(i);
	    int dsid = tmp.Atoi();
	    double N = h_e->GetBinContent(i);
	    Ngen_e[dsid]=N;
   }
   
}   

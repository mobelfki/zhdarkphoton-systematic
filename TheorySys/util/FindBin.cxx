///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2021 CERN for the benefit of the ATLAS collaboration
*/
// FindBin.cxx
// application to find BDT bin with at least one background event
// run as: FindBin samples.txt
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>  
#include "TROOT.h"
#include "TFile.h"
#include <vector>
#include "TheorySys/TheorySys.h"
using namespace std;  

std::map<int,double> Ngen_a;
std::map<int,double> Ngen_d;
std::map<int,double> Ngen_e;

int main(int argc, char* argv []) {
	gROOT->SetBatch(kTRUE);
	if(argc == 1) {
		cerr<< "Input file is required" << endl; 
		return 0;
	}
	
	ifstream input(argv[1]);
	if(!input) {
        		cerr << "Cannot open the File : "<<argv[1]<<endl;
        		return 0;
    	}
    	
    	std::string str;
    	
    	while (std::getline(input, str))
    	{
        		if(str.size() <= 0) continue;
        		vector<std::string> samples;
    		vector<std::string> trees;
        		TString sTr(str);
        		
        		if(sTr.BeginsWith("#")) continue;
        		
        		auto token = sTr.Tokenize(" ");
        		for(int i = 0; i<token->GetEntries()-1; i++)
        		{
        			samples.push_back(token->At(i)->GetName());
        		}
        		cout<<str<<endl;
        		//TString path="/eos/user/h/heljarra/NominalSysTrees";
        		TString path="/eos/home-r/rgarg/Rocky/DarkPhotonMicroNtuples_V17Sys_BDTweights/Var6";
        		cout<< "RUNNING On: " << token->First()->GetName() << endl;
        		TheorySys* obj = new TheorySys(path, samples, token->Last()->GetName());
        		obj->initilize(Ngen_a, Ngen_d, Ngen_e);
        		obj->FindBin();
		delete obj;
   	 }
	
	return 1;
}

///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2021 CERN for the benefit of the ATLAS collaboration
*/
// SysToTXT.cxx
// application to convert sys root to TXT database
// run as: SysToTXT list.txt
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>  
#include "TROOT.h"
#include "TFile.h"
#include <vector>
#include "TheorySys/TheorySys.h"
using namespace std;  

int main(int argc, char* argv []) {
	gROOT->SetBatch(kTRUE);
	if(argc == 1) {
		cerr<< "Input file is required" << endl; 
		return 0;
	}
	
	ifstream input(argv[1]);
	if(!input) {
        		cerr << "Cannot open the File : "<<argv[1]<<endl;
        		return 0;
    	}
    	
    	std::string str;
    	
    	vector<TString> pdf;
    	vector<TString> scale;
    	
    	while (std::getline(input, str))
    	{
        		if(str.size() <= 0) continue;
        		TString sTr(str);
        		
        		
        		if(sTr.BeginsWith("#")) continue;
        	
		if(sTr.BeginsWith("PDFSys")) {
		
			pdf.push_back(sTr); 
		}
	
		if(sTr.BeginsWith("ScaleSys")) {
			
			scale.push_back(sTr);
		
		}
		
	}
	
	for(unsigned int i = 0; i<scale.size(); i++) {
		
		auto token = scale[i].Tokenize("_");
		
		TString name(token->At(1)->GetName());
		
		if(token->GetEntries() == 3) {
		
			name += token->At(2)->GetName();
		}
		
		auto token2 = name.Tokenize(".");
		
		TString t(token2->At(0)->GetName());
		
		cout<<scale[i]<<endl;
		
		TFile file(scale[i], "READ");
		
		for(std::string reg : {"SR", "VR"}) {
		
			for(std::string chan : {"ee", "mm", "ll"}) {
				
				ofstream txtf;
				txtf.open(Form("TheorySys/Scale/Region_%s_Channel_%s_Sample_%s.txt", reg.c_str(), chan.c_str(), t.Data()));
				
				TH1F* h_up = (TH1F*) file.Get(Form("ScalUnc_Region_%s_Channel_%s_Up", reg.c_str(), chan.c_str()));
				TH1F* h_dn = (TH1F*) file.Get(Form("ScalUnc_Region_%s_Channel_%s_Down", reg.c_str(), chan.c_str()));	
				for(int i = 1; i<=h_up->GetNbinsX(); i++) {
		
					double high = h_up->GetBinContent(i);
					double low  = h_dn->GetBinContent(i);
					
					 txtf << Form("%.3f %.3f\n", high, low);						
				}			
				
				txtf.close();
			}
		}	
	}
		
		
	for(unsigned int i = 0; i<pdf.size(); i++) {
		
		auto token = pdf[i].Tokenize("_");
		
		TString name(token->At(1)->GetName());
		
		if(token->GetEntries() == 3) {
		
			name += token->At(2)->GetName();
		}
		
		auto token2 = name.Tokenize(".");
		
		TString t(token2->At(0)->GetName());
		
		TFile file(pdf[i], "READ");
	
		for(std::string reg : {"SR", "VR"}) {
		
			for(std::string chan : {"ee", "mm", "ll"}) {
				
				ofstream txtf;
				txtf.open(Form("TheorySys/PDF/Region_%s_Channel_%s_Sample_%s.txt", reg.c_str(), chan.c_str(), t.Data()));
				
				TH1F* h_up = (TH1F*) file.Get(Form("PDFUnc_Region_%s_Channel_%s_Up", reg.c_str(), chan.c_str()));
				TH1F* h_dn = (TH1F*) file.Get(Form("PDFUnc_Region_%s_Channel_%s_Down", reg.c_str(), chan.c_str()));	
				for(int i = 1; i<=h_up->GetNbinsX(); i++) {
		
					double high = h_up->GetBinContent(i);
					double low  = h_dn->GetBinContent(i);
					
					 txtf << Form("%.3f %.3f\n", high, low);						
				}			
				
				txtf.close();
			}
		}	
	}
			
        		
	
	
	return 0;    	
}  	

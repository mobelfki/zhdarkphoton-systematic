///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2021 CERN for the benefit of the ATLAS collaboration
*/
// TheorySys.h
// header file for class TheorySys
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef TheorySys_H
#define TheorySys_H 1

#include <memory>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <iostream>
#include <cmath>

// ROOT include(s):
#include <TFile.h>
#include <TH1F.h>
#include <TChain.h>
#include <TError.h>
#include <TString.h>
#include <TStopwatch.h>
#include <TSystem.h>
#include "TLorentzVector.h"
#include "TMath.h"
#include "TROOT.h"
#include "TVector3.h"
#include "TSystemDirectory.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"

// local include(s):

// ROOTCore include(s):
#include "SUSYTools/SUSYCrossSection.h"
#include "PathResolver/PathResolver.h"

using namespace std;

class TheorySys {

public:

	TheorySys(TString path, vector<std::string> samples, TString Tree);
	virtual ~TheorySys();
	virtual void initilize(std::map<int,double> N_a, std::map<int,double> N_d, std::map<int,double> N_e);
	virtual void finilize();
	virtual void execute();
	void FindBin();
	void GetTree();

private:

vector<vector<std::string>> m_samples; 
TChain *m_chain;
TString m_TreeName;
TString m_path;
map<std::string,int> vars = {{"Nominal", 0}, {"muF_up", 8}, {"muF_down", 6}, {"muR_up", 9}, {"muR_down", 5}, {"muF_muR_up", 10}, {"muF_muR_down", 4}};
vector<int> color = {1, 46, 36, 48, 38, 42, 32};
enum channels {ee = 0, mm = 1, ll = 2};
vector<std::string> channels_str = {"ee", "mm", "ll"};
vector<std::string> vars_str = {"Nominal", "#mu_{F} = 2", "#mu_{F} = 0.5", "#mu_{R} = 2", "#mu_{R} = 0.5", "#mu_{F} = #mu_{R} = 2", "#mu_{F} = #mu_{R} = 0.5" };
vector<std::string> env_str = {"Nominal", "Up", "Down"};

enum regions {SR = 0, VR = 1};
vector<std::string> reg_str = {"SR", "VR"};

int n_BDT_bins = 6;
vector<double> bins = {0, 0.5, 0.76, 0.83, 0.90, 0.96, 1.};
//vector<double> bins = {0, 0.6, 0.76, 0.90, 0.96, 1.};

TH1F* m_h_ScaleUnc[2][3][7]; // 1 region, 3 channel, 7 variations
TH1F* m_h_ScaleUnc_Env[2][3][3]; //1 region,  3 channel, 3 (Nominal, Up, Down)


TH1F* m_h_PDFUnc[2][3][101]; // 1 region, 3 channel, 100 pdf variations + nominal
TH1F* m_h_PDFUnc_Env[2][3][3];
//TH1F* m_h_BDT_PDF[2][]; // 3 channel, all pdf;

int nChan = 3;
int nVar  = 7;
int nReg  = 2;

TFile* m_output;
TFile* m_ScaleSys;
TFile* m_PDFSys;

TTree* m_Tree;
TFile* m_output_Tree;

TLorentzVector m_lep1; 
TLorentzVector m_lep2; 
TLorentzVector m_ph;

bool m_useAHOI = true;
bool m_fullRun2 = false;


private:

SUSY::CrossSectionDB *SUSYXsecDB;

std::map<int,double> m_Ngen_a;
std::map<int,double> m_Ngen_d;
std::map<int,double> m_Ngen_e;

vector<int> PDF_index;

private:

Float_t mc_wgt;
Float_t weight;
vector<float> *mc_wgts;
bool vy_overlap;
int e_crkVeto;
int y_crkVeto;
int passJetClean;
int n_jet;
int n_bjet;
int n_ph;
int n_baseph;
int n_el;
int n_mu;
int n_basemu;
int n_baseel;
bool passVjets;
float met_tight_tst_et;
float met_tight_tst_phi;
int trigger_lep;
int lep_trig_match;
float mu;
int runNumber;
float BDT_output;
float BDT_output_AHOI;

float m_weight = -99;
bool m_isBaseline = false;
bool m_isLetPt = false;
bool m_isSR = false;
bool m_isAHOI = false;
bool m_isEl = false;
bool m_isMu = false;
float m_mT = -99;
float m_BDT_weight_noAHOI = -99;
float BDT_weight_wAHOI = -99;

vector<float>   *ph_pt;
vector<float>   *ph_phi;
vector<float>   *ph_eta;
vector<float>   *el_charge;
vector<float>   *el_pt;
vector<float>   *el_eta;
vector<float>   *el_phi; 

vector<float>   *mu_charge;
vector<float>   *mu_pt;
vector<float>   *mu_eta;
vector<float>   *mu_phi; 

vector<TString> Branches = {"w", "mcEventWeights", "mcEventWeight", "in_vy_overlap", "n_el_baseline_crackVetoCleaning", "n_ph_baseline_crackVetoCleaning", "passJetCleanTight", "n_jet", "n_bjet", "n_ph", "n_baseph", "n_el", "n_mu", "n_baseel", "n_basemu", "met_tight_tst_et", "passVjetsFilterTauEl", "trigger_lep", "lep_trig_match", "ph_pt", "ph_phi", "ph_eta", "el_charge", "el_pt", "el_eta", "el_phi", "mu_charge", "mu_pt", "mu_eta", "mu_phi", "runNumber", "BDT_weight_noAHOI", "BDT_weight_withAHOI", "met_tight_tst_phi"}; // branches to read for fust looping
inline void linkBranches(TChain* fChain)
{
	mc_wgts = 0;
	ph_pt   = 0;
	ph_phi  = 0;
	ph_eta  = 0;
	
	el_charge = 0;
	el_pt     = 0;
	el_eta    = 0;
	el_phi    = 0;
	
	mu_charge = 0;
	mu_pt     = 0;
	mu_eta    = 0;
	mu_phi    = 0;
	
	fChain->SetBranchStatus("*", 0);
	
	for(auto br : Branches)
	{
		fChain->SetBranchStatus(br, 1); // Set branches to read, to speed-up the process
	}
   	
   	fChain->SetBranchAddress("w", &weight);
   	fChain->SetBranchAddress("mcEventWeights", &mc_wgts);
            fChain->SetBranchAddress("mcEventWeight", &mc_wgt);
   	fChain->SetBranchAddress("in_vy_overlap", &vy_overlap);
   	fChain->SetBranchAddress("n_el_baseline_crackVetoCleaning", &e_crkVeto);
	fChain->SetBranchAddress("n_ph_baseline_crackVetoCleaning", &y_crkVeto);
	fChain->SetBranchAddress("passJetCleanTight", &passJetClean);
	fChain->SetBranchAddress("n_jet", &n_jet);
	fChain->SetBranchAddress("n_bjet", &n_bjet);
	fChain->SetBranchAddress("n_ph", &n_ph);
	fChain->SetBranchAddress("n_baseph", &n_baseph);
	fChain->SetBranchAddress("n_baseel", &n_baseel);
   	fChain->SetBranchAddress("n_basemu", &n_basemu);
	fChain->SetBranchAddress("n_el", &n_el);
	fChain->SetBranchAddress("n_mu", &n_mu);
	fChain->SetBranchAddress("passVjetsFilterTauEl", &passVjets);
	fChain->SetBranchAddress("met_tight_tst_et", &met_tight_tst_et);
	fChain->SetBranchAddress("met_tight_tst_phi", &met_tight_tst_phi);	
	fChain->SetBranchAddress("trigger_lep", &trigger_lep);
	fChain->SetBranchAddress("lep_trig_match", &lep_trig_match);
	fChain->SetBranchAddress("averageIntPerXing", &mu);
	
	fChain->SetBranchAddress("el_charge", &el_charge);
	fChain->SetBranchAddress("mu_charge", &mu_charge);
	
   	fChain->SetBranchAddress("el_pt", &el_pt);
   	fChain->SetBranchAddress("el_phi", &el_phi);
   	fChain->SetBranchAddress("el_eta", &el_eta);
   	
   	fChain->SetBranchAddress("mu_pt", &mu_pt);
   	fChain->SetBranchAddress("mu_phi", &mu_phi);
   	fChain->SetBranchAddress("mu_eta", &mu_eta);
   	
   	fChain->SetBranchAddress("ph_pt", &ph_pt);
   	fChain->SetBranchAddress("ph_phi", &ph_phi);
   	fChain->SetBranchAddress("ph_eta", &ph_eta);
   	fChain->SetBranchAddress("runNumber", &runNumber);
   	
   	fChain->SetBranchAddress("BDT_weight_noAHOI", &BDT_output);
   	fChain->SetBranchAddress("BDT_weight_withAHOI", &BDT_output_AHOI);
   	return;
}

inline bool isBaseline() {

	if(passVjets < 0.5) return false;
	if(!(passOverlap() > 0)) return false;
	if(!(e_crkVeto == 0 && y_crkVeto == 0)) return false;
	if(passJetClean != 1) return false;
	if(!(n_jet <= 2)) return false;
	if(!passTrigger()) return false;
	 
	return true;
}

inline bool isSR(int c=-1) {


	if(!(n_ph == 1 && n_baseph == 0)) return false;
	if(c==channels::ee && !isEleChannel()) return false;
	if(c==channels::mm && !isMuChannel()) return false;
	//if(n_bjet != 0) return false;
	TLorentzVector m_Z = RecZboson();
	
	RecPhoton();
	
	//if(m_lep1.Pt() < 27 ) return false;
	//if(m_lep2.Pt() < 20 ) return false;
	
	if(m_useAHOI)
	{
	
		if(!passAHOI(m_Z.M(), m_ph.Pt(), (m_Z+m_ph).M(), met_tight_tst_et*1e-3)) return false;
	}else{
		if(m_Z.M() > 116 || m_Z.M() < 66) return false;
	}
	return true;
}

inline bool isVR(int c=-1) {


	if(!(n_ph == 1 && n_baseph == 0)) return false;
	if(c==channels::ee && !isEleChannel()) return false;
	if(c==channels::mm && !isMuChannel()) return false;
	//if(n_bjet != 0) return false;
	TLorentzVector m_Z = RecZboson();
	
	RecPhoton();
	
	//if(m_lep1.Pt() < 27 ) return false;
	//if(m_lep2.Pt() < 20 ) return false;
	
	TLorentzVector MET;
	MET.SetPtEtaPhiM(met_tight_tst_et/1.0e3, 0, met_tight_tst_phi, 0);
	
	float dphi = fabs(MET.DeltaPhi(m_Z+m_ph));
	
	if(!passAHOIVR(m_Z.M(), m_ph.Pt(), (m_Z+m_ph).M(), met_tight_tst_et*1e-3, dphi)) return false;
	
	return true;
}

inline bool isCRtt () {

	if(n_ph != 0) return false;
	if(n_el != 1) return false;
	if(n_mu != 1) return false;
	
	if(!(n_bjet >= 1)) return false;
	
	TLorentzVector m_Z = RecZboson();
	if(m_lep1.Pt() < 27 ) return false;
	if(m_lep2.Pt() < 20 ) return false;
	if(m_Z.M() > 116 || m_Z.M() < 66) return false;
	
	return true;
}

inline bool passAHOI(float mll, float ph_pt, float mllg, float met) {

	if(met < 60) return false;
	if(mll > 116) return false;
	if(mll < 76) return false;
	if(ph_pt < 25) return false;
	if(mllg < 100) return false;
	
	return true;

}

inline bool passAHOIVR(float mll, float ph_pt, float mllg, float met, float dphi) {

	if( met < 40) return false;
	if( met > 60) return false;
	if(mll > 116) return false;
	if(mll < 76) return false;
	if(ph_pt < 25) return false;
	if(mllg < 100) return false;
	if(dphi < 2.4) return false;
	
	return true;
	
	
}

inline bool isEleChannel() {
	
	if(n_el == 2 && n_baseel == 2 && n_basemu == 0) return isBaseline() && oppositeCharge(el_charge) && true;
	
	return false;
}

inline bool isMuChannel() {
	
	if(n_mu == 2 && n_baseel == 0 && n_basemu == 2) return isBaseline() && oppositeCharge(mu_charge) && true;
	
	return false;
}

inline bool passTrigger() {

	int trigger_lep_new = ((trigger_lep&0x1)>0) ? 1 : 0;
    	// trigger matching is only done for Z leptons. Not for base leptons
    	if(lep_trig_match <1) trigger_lep_new=0; // was <0.5 changed to <1
    	if(trigger_lep_new==0 && (trigger_lep&0x20)==0x20) trigger_lep_new = 2; //dimu
    	if(trigger_lep_new==0 && (trigger_lep&0x40)==0x40) trigger_lep_new = 3; //dimu
    	if(trigger_lep_new==0 && (trigger_lep&0x400)==0x400) trigger_lep_new = 4; //diele
	
	if(trigger_lep_new> 0) return true;
	return false;
}

inline int passOverlap() {

	if(isTop() && vy_overlap) return 0;
	if(isVjets() && vy_overlap) return 0;
	if(isVV() && vy_overlap) return 0;
	
	return 1;
	
}
inline bool isTop() {

	return (runNumber == 410472);
}

inline bool isVjets() {

	vector<int> Vjets = {364107, 364108, 364109, 364104, 364105, 364106, 364110, 364100, 364101, 364102, 364103, 364113, 364112, 364111, 364122, 364123, 364119, 364121, 364126, 364127, 364124, 364125, 364117, 364120, 364115, 364116, 364118, 364114, 364131, 364130, 364133, 364132, 364135, 364134, 364137, 364136, 364139, 364138, 364128, 364129, 364140, 364141, 308092, 308093, 308094}; // Zqcd + Zewk;
	
	bool isvjet = false;
	
	for(int run : Vjets)
	{
		isvjet += (run == runNumber);
	}	
	return isvjet;
}
inline bool isZgQCD() {
	
	vector<int> ZgQCD = {700011, 700012, 700013, 700018, 700019, 700020};
	
	bool isZg = false;
	
	for(int run : ZgQCD)
	{
		isZg += (run == runNumber);
	}	
	return isZg;
}
inline bool isZgEWK() {

	vector<int> ZgEWK = {363266, 363267, 363268, 363269};
	
	bool isZ = false;
	
	for(int run : ZgEWK)
	{
		isZ += (run == runNumber);
	}	
	return isZ;

}

inline bool isZQCDmumu() {

	vector<int> ZQCD = {364107, 364108, 364109, 364104, 364105, 364106, 364110, 364101, 364102, 364103, 364113, 364112, 364111};
	
	bool isZ = false;
	
	for(int run : ZQCD)
	{
		isZ += (run == runNumber);
	}	
	return isZ;

}


inline bool isVV() {
	
	if( runNumber == 345718 || runNumber == 345723 || runNumber == 364283 || runNumber == 364284 || runNumber == 364285) return true;
	
	return false;
}

inline bool isttV() {

	vector<int> ttV = {410155, 410156, 410218, 410219, 410220};
	bool isttV = false;
	
	for(int run : ttV)
	{
		isttV += (run == runNumber);
	} 
	
	return isttV;
}

inline bool isVVy() {

	vector<int> VVy = {366160, 366161, 366162};
	bool isVVy = false;
	for(int run : VVy) 
	{
		isVVy += (run == runNumber);
	}
	
	return isVVy;
}
inline bool isttbar() {
	
	vector<int> ttbar = {410472};

	bool isttbar = false;
	for(int run : ttbar) {
		
		isttbar += (run == runNumber);
	}
	
	return isttbar;
}

inline bool isWty() { 

	vector<int> Wty = {412006, 412120};
	
	bool isWty = false;
	for(int run : Wty) {
	
		isWty += (run == runNumber);
	}  
	
	return isWty;
}

inline bool isWgQCD() {

	vector<int> WgQCD = {700015, 700016, 700017, 700022, 700023, 700024};
	
	bool isWgQCD = false;
	for(int run : WgQCD) {
		
		isWgQCD += (run == runNumber);
	}
	
	return isWgQCD;
} 

inline bool isWgEWK() {
	
	vector<int> WgEWK = {363270, 363271, 363272};
	
	bool isWgEWK = false;
	for(int run : WgEWK) {
	
		isWgEWK += (run == runNumber);
	}
	
	return isWgEWK;
}

inline bool isttHZy() {

	vector<int> ttHZy = {346198};
	bool isttHZy = false;
	for(int run : ttHZy) {
		isttHZy += (run == runNumber);
	}
	return isttHZy;
}
inline bool isVHZy() {
	
	vector<int> VHZy = {345320, 345321};
	bool isVHZy = false;
	for(int run : VHZy) {
		isVHZy += (run == runNumber);
	}
	return isVHZy;
}

inline bool isVHZll() {

	vector<int> VHZll = {600060};
	
	bool isVHZll = false;
	for(int run : VHZll) {
		isVHZll += (run == runNumber);
	}
	return isVHZll;
}

inline bool isHyGr() {
	
	vector<int> HyGr = {600059};
	
	bool isHyGr = false;
	
	for(int run : HyGr) {
	
		isHyGr += (run == runNumber);
	}

	return isHyGr;
}

inline bool istop1() {

	vector<int> top1 = {410644, 410645, 410658, 410659};
	
	bool istop1 = false;
	for(int run : top1) {
	
		istop1 += (run == runNumber);
	}
	
	return istop1;
}

inline bool oppositeCharge(vector<float> *charge)
{
	return (charge->at(0)*charge->at(1) < 0);
}

inline TLorentzVector RecZboson()
{
	if(isEleChannel())
	{		
		m_lep1.SetPtEtaPhiM(el_pt->at(0)/1.0e3, el_eta->at(0), el_phi->at(0), 0.000511);
		m_lep2.SetPtEtaPhiM(el_pt->at(1)/1.0e3, el_eta->at(1), el_phi->at(1), 0.000511);
		
		return m_lep1 + m_lep2;
	}
	
	if(isMuChannel())
	{
		m_lep1.SetPtEtaPhiM(mu_pt->at(0)/1.0e3, mu_eta->at(0), mu_phi->at(0), 0.10566);
		m_lep2.SetPtEtaPhiM(mu_pt->at(1)/1.0e3, mu_eta->at(1), mu_phi->at(1), 0.10566);
		
		return m_lep1 + m_lep2;
	}
	
	return TLorentzVector(0,0,0,0);
}

inline double getLumi(TChain* fchain)
{
	TString name(fchain->GetFile()->GetName());
	if(name.Contains("/mc16a/"))
	{
		return 36214.96;
		
	}else if(name.Contains("/mc16d/"))
	{
		return 44307.4;
	}else if(name.Contains("/mc16e/"))
	{
		return 58450.1;
	
	}else {
		cout<<"Error: Lumi set to zero "<<endl;
		return 0.;
	}
	return 0;
}

inline bool PassRegion(int reg, int chan) {

	switch(reg) {
	
	case 0:
		switch(chan) {
			case 0:
				return isSR(channels::ee);
			case 1:
				return isSR(channels::mm);
			case 2:
				return isSR(channels::ee) or isSR(channels::mm);
			default: 
				return false;
		}
	case 1:
		switch(chan) {
			case 0:
				return isVR(channels::ee);
			case 1:
				return isVR(channels::mm);
			case 2:
				return isVR(channels::ee) or isVR(channels::mm);
			default: 
				return false;
		}	
	default:
		return false;
	
	}

	return false;
} 

inline void RecPhoton()
{
	m_ph.SetPtEtaPhiM(ph_pt->at(0)/1.0e3, ph_eta->at(0), ph_phi->at(0), 0.0);
	return;
}

void GetMaxMinVar(TH1F* h_input[], TH1F* &Nominal, TH1F* &Max, TH1F* &Min, int nvar=7, int nominal = 0, bool isPDF = false);
void PlotEnvlop(TH1F* Nominal, TH1F* Max, TH1F* Min, int chan, int reg, TString tag);
void PrintSystematic(TH1F* Nominal, TH1F* Max, TH1F* Min, int chan, int reg); 
void SetVarsIndex();
vector<int> getPDFIndex();

inline Double_t* createBins(vector<double> bins, int nbins)
{
	Double_t* Bins = new Double_t[nbins+1];
	for(int i = 0; i<=nbins; i++)
	{
		Bins[i] = bins[i];
	}
	return Bins;
}

inline float GetBr(int fRunNumber) {

	 float BrWeight = 1.0;
	 
	 if(fRunNumber==345320 || fRunNumber==345321 || fRunNumber==345322) { BrWeight = 0.00015708; } //VHZy  HZgamBkgWeight
	 
	 else if(fRunNumber==346198){ BrWeight = 0.00015708; } //ttHZy was 0.0015 but the JO has the Z decay 
	 
	 else if(fRunNumber==345833){ BrWeight = 0.00015708; }//VBFHZy
	 
	 else if(fRunNumber==345316){ BrWeight = 0.00015708; } //ggHZy
	 
          	 else if(fRunNumber==345319){ BrWeight = 0.00227; } //Hyy
          	 
             else if(fRunNumber==600060){ BrWeight = 0.00046508; } //ZH Zy Z -> ll,nunu
             
             else if(fRunNumber==600059 || fRunNumber==600301 || fRunNumber==600302 || fRunNumber==600303 || fRunNumber==600304 || fRunNumber==600305)	{ BrWeight = 0.05; }

	return BrWeight;
}


};

#endif //> !TheorySys_H

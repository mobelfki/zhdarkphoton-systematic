#!/usr/bin/env python

import pandas as pd
import numpy as np
from math import pi as PI
import math
from argparse import ArgumentParser
import sys
import os
from collections import OrderedDict
from tools.LateXTable import MakeScaleSysTable, MakePDFSysTable


def getArgs():
	"""
	Get arguments from command line.
	"""
	args = ArgumentParser(description="Argumetns for NTupleToNumpy for BDT")
	args.add_argument('-f', '--file', action='store', required=True, help='path to txt files')
	return args.parse_args()


def main():
	
	args=getArgs()
	path = args.file
	
	Samples = ['HyGrNominal', 'VH125ZyNominal', 'VVyNominal', 'WgewkNominal', 'WgstrongNominal', 'WtyNominal', 'top1Nominal', 'ttH125ZyNominal', 'ttVNominal', 'ttbarNominal']
	
	dic = {'HyGrNominal': 'signal', 'VH125ZyNominal': 'VH(Z$\gamma$)', 'VVyNominal': 'VV$\gamma$', 'WgewkNominal': 'W$\gamma_{ewk}$', 'WgstrongNominal': 'W$\gamma_{qcd}$', 'WtyNominal': 'Wt$\gamma$', 'top1Nominal': 'single Top', 'ttH125ZyNominal': 'ttH(Z$\gamma$)', 'ttVNominal': 'ttV', 'ttbarNominal': 'ttbar'}
	
	for sys in ['Scale', 'PDF']:
		
		for chan in ['ee', 'mm', 'll']:
			
			for reg in ['SR', 'VR']:
			
				dics = {}
				
				for sample in Samples:
			
					f = open(path+'/'+sys+'/'+'Region_'+reg+'_Channel_'+chan+'_Sample_'+sample+'.txt', 'r')
					
					lines = f.readlines()
					
					values = []
					
					for l in lines: 
						l = l.split(' ')
						
						up   = float(l[0])
						down = float(l[1])
						
						if up == -1 or math.isnan(up):
							up  = 0
						if down == -1 or math.isnan(down):
							down = 0
							
							
						values.append([up*100, down*100])
					
					dics.update({dic[sample]: [values[0], values[1], values[2], values[3], values[4], values[5]]})
				if(sys == 'Scale'):	
					MakeScaleSysTable(dics, sys+'_'+'Region_'+reg+'_Channel_'+chan+'.tex')
				if(sys == 'PDF'):
					MakePDFSysTable(dics, sys+'_'+'Region_'+reg+'_Channel_'+chan+'.tex')	
				os.system('pdflatex '+sys+'_'+'Region_'+reg+'_Channel_'+chan+'.tex > log.log')
	os.system('rm *.log *.aux *.out')
				
if __name__ == '__main__':
	main()

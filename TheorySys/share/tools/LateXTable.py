#!/usr/bin/env python

from math import *

def MakeTEXTable(dic, name): 
	
	latexFile = open(name, "w+")
	latexFile.write("\\documentclass[12pt]{article} \n")
	latexFile.write("\\usepackage{amsmath}\n")
	latexFile.write("\\usepackage{graphicx}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{multirow}\n")
	latexFile.write("\\usepackage[latin1]{inputenc}\n")
	latexFile.write("\\begin{document}\n")
	latexFile.write("\\begin{table}[]\n")
	latexFile.write("\\resizebox{\\textwidth}{!}{  \n")
	latexFile.write("\\begin{tabular}{lcccccc} \n")
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("Process & Baseline	& SR & SR+Lep ${p_T}$ &	SR+AHOI & SR+BDT & SR+AHOI+BDT \\\ \n")
	latexFile.write("\\hline  \\hline \n")
	for Process, Yield in dic.items():	
		latexFile.write(" %s & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f \\\ \n"%(Process, Yield['Baseline'][0], Yield['Baseline'][1], Yield['SR'][0], Yield['SR'][1], Yield['SR+Lep'][0], Yield['SR+Lep'][1], Yield['SR+AHOI'][0], Yield['SR+AHOI'][1], Yield['SR+BDT'][0], Yield['SR+BDT'][1], Yield['SR+AHOI+BDT'][0], Yield['SR+AHOI+BDT'][1]))
	
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("\\end{tabular}\n")
	latexFile.write("}\n")
	latexFile.write("\\end{table}\n")
	latexFile.write("\\end{document}\n")
	latexFile.close()
	
def MakeScaleSysTable(dic, name):

	latexFile = open(name, "w+")
	latexFile.write("\\documentclass[12pt]{article} \n")
	latexFile.write("\\usepackage{amsmath}\n")
	latexFile.write("\\usepackage{graphicx}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{multirow}\n")
	latexFile.write("\\usepackage[latin1]{inputenc}\n")
	latexFile.write("\\begin{document}\n")
	latexFile.write("\\begin{table}[]\n")
	latexFile.write("\\resizebox{\\textwidth}{!}{  \n")
	latexFile.write("\\begin{tabular}{lcccccc} \n")
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("Process & Bin1 & Bin2 & Bin3 &	Bin4 & Bin5 & Bin6 \\\ \n")
	latexFile.write("\\hline  \\hline \n")
	for Process, Sys in dic.items():
	
		latexFile.write(" %s & $ ^{+%.1f}_{%.1f} $ & $ ^{+%.1f}_{%.1f} $ & $ ^{+%.1f}_{%.1f} $ & $ ^{+%.1f}_{%.1f} $ & $ ^{+%.1f}_{%.1f} $ & $ ^{+%.1f}_{%.1f} $ \\\ \n"%(Process, Sys[0][0], Sys[0][1], Sys[1][0], Sys[1][1], Sys[2][0], Sys[2][1], Sys[3][0], Sys[3][1], Sys[4][0], Sys[4][1], Sys[5][0], Sys[5][1]  ))
		#latexFile.write(" %s & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f \\\ \n"%(Process, Yield['Baseline'][0], Yield['Baseline'][1], Yield['SR'][0], Yield['SR'][1], Yield['SR+Lep'][0], Yield['SR+Lep'][1], Yield['SR+AHOI'][0], Yield['SR+AHOI'][1], Yield['SR+BDT'][0], Yield['SR+BDT'][1], Yield['SR+AHOI+BDT'][0], Yield['SR+AHOI+BDT'][1]))
	
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("\\end{tabular}\n")
	latexFile.write("}\n")
	latexFile.write("\\end{table}\n")
	latexFile.write("\\end{document}\n")
	latexFile.close()
	
def MakePDFSysTable(dic, name):

	latexFile = open(name, "w+")
	latexFile.write("\\documentclass[12pt]{article} \n")
	latexFile.write("\\usepackage{amsmath}\n")
	latexFile.write("\\usepackage{graphicx}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{hyperref}\n")
	latexFile.write("\\usepackage{multirow}\n")
	latexFile.write("\\usepackage[latin1]{inputenc}\n")
	latexFile.write("\\begin{document}\n")
	latexFile.write("\\begin{table}[]\n")
	latexFile.write("\\resizebox{\\textwidth}{!}{  \n")
	latexFile.write("\\begin{tabular}{lcccccc} \n")
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("Process & Bin1 & Bin2 & Bin3 &	Bin4 & Bin5 & Bin6 \\\ \n")
	latexFile.write("\\hline  \\hline \n")
	for Process, Sys in dic.items():
	
		latexFile.write(" %s & $ \pm %.1f $ & $ \pm %.1f $ & $ \pm %.1f $ & $ \pm %.1f $ & $ \pm %.1f $ & $ \pm %.1f $ \\\ \n"%(Process, Sys[0][0], Sys[1][0], Sys[2][0], Sys[3][0], Sys[4][0], Sys[5][0]))
		#latexFile.write(" %s & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f & %.2f $\pm$ %.2f \\\ \n"%(Process, Yield['Baseline'][0], Yield['Baseline'][1], Yield['SR'][0], Yield['SR'][1], Yield['SR+Lep'][0], Yield['SR+Lep'][1], Yield['SR+AHOI'][0], Yield['SR+AHOI'][1], Yield['SR+BDT'][0], Yield['SR+BDT'][1], Yield['SR+AHOI+BDT'][0], Yield['SR+AHOI+BDT'][1]))
	
	latexFile.write("\\hline  \\hline \n")
	latexFile.write("\\end{tabular}\n")
	latexFile.write("}\n")
	latexFile.write("\\end{table}\n")
	latexFile.write("\\end{document}\n")
	latexFile.close()		

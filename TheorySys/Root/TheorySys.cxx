///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2021 CERN for the benefit of the ATLAS collaboration
*/
// TheorySys.cxx
// source file for class TheorySys
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#include "TheorySys/TheorySys.h"


TheorySys::TheorySys(TString path, vector<std::string> samples, TString Tree) {

	m_samples.clear();
	m_samples.push_back(samples);
	m_path = path;
	m_TreeName = Tree;
}


TheorySys::~TheorySys() {


}

void TheorySys::initilize(std::map<int,double> N_a, std::map<int,double> N_d, std::map<int,double> N_e) {


	m_Ngen_a = N_a; 
	m_Ngen_d = N_d;
	m_Ngen_e = N_e;
	
	Double_t* BDT_bins = createBins(bins, n_BDT_bins);
	
	std::string XsecPath = PathResolverFindCalibFile("TheorySys/PMGxsecDB_mc16.txt");
	SUSYXsecDB = new SUSY::CrossSectionDB(XsecPath);
	
	m_chain = new TChain(m_TreeName);
	for(auto file : m_samples[0])
	{
		m_chain->AddFile(m_path+"/mc16a/"+file,-1,m_TreeName);
		m_chain->AddFile(m_path+"/mc16d/"+file,-1,m_TreeName);
		m_chain->AddFile(m_path+"/mc16e/"+file,-1,m_TreeName);
	}
	
	linkBranches(m_chain);
	
	
	m_output = new TFile("TheorySys_"+m_TreeName+"_output.root", "RECREATE");
	m_ScaleSys = new TFile("ScaleSys_"+m_TreeName+".root", "RECREATE");
	m_PDFSys = new TFile("PDFSys_"+m_TreeName+".root", "RECREATE");
	
	for( auto reg : {regions::SR, regions::VR}) {
		for( auto chan : {channels::ee, channels::mm, channels::ll} ) {
			int i = 0;
			for(const auto& v : vars) {
				m_h_ScaleUnc[reg][chan][i] = new TH1F(Form("h_Region_%s_Channel_%s_ScaleUnc_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), v.first.c_str()), Form("h_Region_%s_Channel_%s_ScaleUnc_%s; BDT Output; N Event", reg_str[reg].c_str(), channels_str[chan].c_str(), v.first.c_str()), n_BDT_bins, BDT_bins);
	
				m_h_ScaleUnc[reg][chan][i]->SetDirectory(m_output);
				m_h_ScaleUnc[reg][chan][i]->SetStats(0);
				m_h_ScaleUnc[reg][chan][i]->SetLineWidth(2);
				m_h_ScaleUnc[reg][chan][i]->SetLineColor(color[i]);
				i++;
			}
			
			for(int j = 0; j<101; j++) {
			
				m_h_PDFUnc[reg][chan][j] = new TH1F(Form("h_Region_%s_Channel_%s_PDFUnc_%i", reg_str[reg].c_str(), channels_str[chan].c_str(), j), Form("h_Region_%s_Channel_%s_PDFUnc_%i; BDT Output; N Event", reg_str[reg].c_str(), channels_str[chan].c_str(), j), n_BDT_bins, BDT_bins);
				m_h_PDFUnc[reg][chan][j]->SetDirectory(m_output);
				m_h_PDFUnc[reg][chan][j]->SetStats(0);
				m_h_PDFUnc[reg][chan][j]->SetLineWidth(2);
				m_h_PDFUnc[reg][chan][j]->SetLineColor(j+1);
				
			}
			
			for(int j = 0; j<3; j++) {
				m_h_ScaleUnc_Env[reg][chan][j] =  new TH1F(Form("h_Region_%s_Channel_%s_ScaleUnc_Env_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), env_str[j].c_str()), Form("h_Region_%s_Channel_%s_ScaleUnc_Env_%s; BDT Output; N Event", reg_str[reg].c_str(), channels_str[chan].c_str(), env_str[j].c_str()), n_BDT_bins, BDT_bins);
				m_h_ScaleUnc_Env[reg][chan][j]->SetStats(0);
				m_h_ScaleUnc_Env[reg][chan][j]->SetLineWidth(2);
				m_h_ScaleUnc_Env[reg][chan][j]->SetDirectory(m_output);
				
				m_h_PDFUnc_Env[reg][chan][j] = new TH1F(Form("h_Region_%s_Channel_%s_PDFUnc_Env_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), env_str[j].c_str()), Form("h_Region_%s_Channel_%s_PDFUnc_Env_%s; BDT Output; N Event", reg_str[reg].c_str(), channels_str[chan].c_str(), env_str[j].c_str()), n_BDT_bins, BDT_bins);
				
				m_h_PDFUnc_Env[reg][chan][j]->SetStats(0);
				m_h_PDFUnc_Env[reg][chan][j]->SetLineWidth(2);
				m_h_PDFUnc_Env[reg][chan][j]->SetDirectory(m_output);
				
			} 
		}
	}
	
return;	
}

void TheorySys::finilize() {

	gROOT->SetBatch(kTRUE);
	
	for(auto reg : {regions::SR, regions::VR}) {
		for(auto chan : {channels::ee, channels::mm, channels::ll}) {
	
			TCanvas c(Form("SubPlot_ScalUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), Form("All Scale Vars in %s channel", channels_str[chan].c_str()), 850, 850);
						
			TLegend legend(0.8,0.7,0.48,0.9);
			legend.SetFillColor(0);
			legend.SetFillStyle(0);
			legend.SetBorderSize(0);
			legend.SetTextSize(0.03);
			legend.SetTextAlign(11);
			legend.SetHeader(Form("Channel: %s, Region: %s",channels_str[chan].c_str(), reg_str[reg].c_str()));
			
			TPad up(Form("PadUp_ScalUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Up", 0, 0.3, 1, 1.0);
			c.cd();
			c.SetTickx();
			c.SetTicky();
			up.Draw();
			up.cd();
			up.SetLogy(1);
			
			for(int i = 0; i<nVar; i++) {
				legend.AddEntry(m_h_ScaleUnc[reg][chan][i], vars_str[i].c_str());
				m_h_ScaleUnc[reg][chan][i]->Draw("same HIST");
			}
			
			legend.Draw("same");
			
			
			TPad down(Form("PadDown_ScalUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Down", 0, 0.05, 1, 0.3);
			c.cd();
			down.Draw();
			down.cd();
			
			for(int i = 0; i<nVar; i++)
			{
				TH1F* h_Ratio = (TH1F*) m_h_ScaleUnc[reg][chan][i]->Clone(); 
				h_Ratio->SetName(Form("%s_%s", m_h_ScaleUnc[reg][chan][i]->GetName(),"RatioToNominal"));
				h_Ratio->SetDirectory(m_output);
				h_Ratio->Divide(m_h_ScaleUnc[reg][chan][0]);
				h_Ratio->GetYaxis()->SetTitle("Var/Nominal");
				h_Ratio->GetYaxis()->SetRangeUser(0.6, 1.4);
				h_Ratio->Draw("same HIST");
			}
			
			c.SaveAs(Form("SubPlot_ScalUnc_AllVars_Region_%s_Channel_%s_Sample_%s.pdf", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()));
			
			GetMaxMinVar(m_h_ScaleUnc[reg][chan], m_h_ScaleUnc_Env[reg][chan][0], m_h_ScaleUnc_Env[reg][chan][1], m_h_ScaleUnc_Env[reg][chan][2], nVar, 0, false);

			PlotEnvlop(m_h_ScaleUnc_Env[reg][chan][0], m_h_ScaleUnc_Env[reg][chan][1], m_h_ScaleUnc_Env[reg][chan][2], chan, reg, "ScalUnc");
		}
	}
	
	
	// PDF uncert. 
	
	for(auto reg : {regions::SR, regions::VR}) {
		for(auto chan : {channels::ee, channels::mm, channels::ll}) {
	
			TCanvas c(Form("SubPlot_PDFUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), Form("All Scale Vars in %s channel", channels_str[chan].c_str()), 850, 850);
						
			TLegend legend(0.8,0.7,0.48,0.9);
			legend.SetFillColor(0);
			legend.SetFillStyle(0);
			legend.SetBorderSize(0);
			legend.SetTextSize(0.03);
			legend.SetTextAlign(11);
			legend.SetHeader(Form("Channel: %s, Region: %s",channels_str[chan].c_str(), reg_str[reg].c_str()));
			
			TPad up(Form("PadUp_PDFUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Up", 0, 0.3, 1, 1.0);
			c.cd();
			c.SetTickx();
			c.SetTicky();
			up.Draw();
			up.cd();
			up.SetLogy(1);
			
			for(int i = 0; i<101; i++) {
				legend.AddEntry(m_h_PDFUnc[reg][chan][i], Form("pdf_%i",i));
				m_h_PDFUnc[reg][chan][i]->Draw("same HIST");
			}
			
			legend.Draw("same");
			
			
			TPad down(Form("PadDown_PDFUnc_Region_%s_Channel_%s_Sample_%s", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Down", 0, 0.05, 1, 0.3);
			c.cd();
			down.Draw();
			down.cd();
			
			for(int i = 0; i<101; i++)
			{
				TH1F* h_Ratio = (TH1F*) m_h_PDFUnc[reg][chan][i]->Clone(); 
				h_Ratio->SetName(Form("%s_%s", m_h_PDFUnc[reg][chan][i]->GetName(),"RatioToNominal"));
				h_Ratio->SetDirectory(m_output);
				h_Ratio->Divide(m_h_PDFUnc[reg][chan][0]);
				h_Ratio->GetYaxis()->SetTitle("Var/Nominal");
				h_Ratio->GetYaxis()->SetRangeUser(0.6, 1.4);
				h_Ratio->Draw("same HIST");
			}
			
			c.SaveAs(Form("SubPlot_PDFUnc_AllVars_Region_%s_Channel_%s_Sample_%s.pdf", reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()));
			
			GetMaxMinVar(m_h_PDFUnc[reg][chan], m_h_PDFUnc_Env[reg][chan][0], m_h_PDFUnc_Env[reg][chan][1], m_h_PDFUnc_Env[reg][chan][2], 100, 0, true);

			PlotEnvlop(m_h_PDFUnc_Env[reg][chan][0], m_h_PDFUnc_Env[reg][chan][1], m_h_PDFUnc_Env[reg][chan][2], chan, reg, "PDFUnc");
		}
		
	}
	
	
	m_output->Write();
	m_output->Close();
	
	m_ScaleSys->Write();
	m_ScaleSys->Close();
	
	m_PDFSys->Write();
	m_PDFSys->Close();
	

return;
}

void TheorySys::execute() {

int Nevent = m_chain->GetEntriesFast();
for(int i = 0; i<Nevent; i++) {

	m_chain->GetEntry(i);
	
	if(i%10000 == 0) cout<< " Events "<<i<<"/"<<Nevent<<endl;
	
	float lumi = getLumi(m_chain); 
	//if(m_fullRun2) lumi = 139000;
	
	float Xsec = SUSYXsecDB->xsectTimesEff(runNumber);
	
	float Total_wgt = (weight*GetBr(runNumber)*lumi)/mc_wgt;
	
	float N = m_Ngen_a[runNumber] + m_Ngen_d[runNumber] + m_Ngen_e[runNumber]; // not needed since the systematic is applied on the yield
	
	float XsecN = Xsec/N;
	
	if(not isBaseline()) continue;
	
	SetVarsIndex();
	PDF_index = getPDFIndex();
	
	//cout<< mc_wgt << "  " << mc_wgts->at(0) << " " << mc_wgts->at(85) << endl;
	
	//cout<<PDF_index.size() <<endl;
	
	if(mc_wgt != mc_wgts->at(0)) cout<<"SOMETHING Here"<<endl;
	
	for(auto reg : {regions::SR, regions::VR}) {
		for(auto chan : {channels::ee, channels::mm, channels::ll}) {
			if(not PassRegion(reg, chan)) continue;
			if(m_lep1.Pt() < 27 ) continue;
			if(m_lep2.Pt() < 20 ) continue;
			int j = 0;
			for(const auto& v : vars) {
				m_h_ScaleUnc[reg][chan][j]->Fill(BDT_output_AHOI, Total_wgt*mc_wgts->at(v.second)); // use BDT+AHOI
				j++;
			}
			
			if(PDF_index.size() != 101) cout<<" SOMETHING WRONG WITH PDFs"<<endl;
			for(unsigned int l = 0; l<PDF_index.size(); l++) {
				//cout<< l << " "<< PDF_index[l]<< " " << m_h_PDFUnc[reg][chan][l]->GetName() <<endl;
				if(isZQCDmumu() or mc_wgts->size() < 100) continue; 
				m_h_PDFUnc[reg][chan][l]->Fill(BDT_output_AHOI, Total_wgt*mc_wgts->at(PDF_index[l])); // use BDT+AHOI
			}	
		}
	}
	

}

return;

} 

void TheorySys::GetMaxMinVar(TH1F* h_input[], TH1F* &Nominal, TH1F* &Max, TH1F* &Min, int nvar, int nominal, bool isPDF) {

	
	if(isPDF){ 
		TH1F* h_Nom = (TH1F*) h_input[nominal];		
		
		for(int i = 1; i<nvar; i++) {
			h_Nom->Add(h_input[i]);
		}
		h_Nom->Scale(1./nvar); 
			
		for(int bin = 1; bin<=h_input[nominal]->GetNbinsX(); bin++)
		{	
			double cnt = h_Nom->GetBinError(bin)/h_Nom->GetBinContent(bin);
			
			Nominal->SetBinContent(bin, cnt);
			Max->SetBinContent(bin, 1+cnt);
			Min->SetBinContent(bin, 1-cnt);
		}
		
		return;
	}
	
	
	// Some x-checks 
	if(!isPDF) {
	if(h_input[nominal]->GetNbinsX() != Nominal->GetNbinsX()){ cout<<"Error: different binning!"<<endl; return;}
	
	for(int bin = 1; bin<=h_input[nominal]->GetNbinsX(); bin++)
	{
	
		double min = h_input[nominal]->GetBinContent(bin); 
		double max = h_input[nominal]->GetBinContent(bin);
	
		for(int i = 0; i<nvar; i++) { // consider the 7 variations
			
			double val = h_input[i]->GetBinContent(bin);

			if( val >= max) max = val;
			if( val <= min) min = val;			
		}
		
		Nominal->SetBinContent(bin, h_input[nominal]->GetBinContent(bin));
		Max->SetBinContent(bin, max);
		Min->SetBinContent(bin, min);
	}
		return;
	}
	return;
}

void TheorySys::PlotEnvlop(TH1F* Nominal, TH1F* Max, TH1F* Min, int chan, int reg, TString tag) {

	TCanvas c(Form("SubPlot_%s_Env_Region_%s_Channel_%s_Sample_%s", tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), Form("Env Scale Vars in %s channel", channels_str[chan].c_str()), 850, 850);
	
	TPad up(Form("PadUp_%s_Env_Region_%s_Channel_%s_Sample_%s", tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Up", 0, 0.3, 1, 1.0);
	
	c.cd();
	c.SetTickx();
	c.SetTicky();
	up.Draw();
	up.cd();
	up.SetLogy(1);
	
	Nominal->SetLineColor(1);
	Max->SetLineColor(2);
	Min->SetLineColor(4);
	
	Nominal->SetLineWidth(2);
	Max->SetLineWidth(2);
	Min->SetLineWidth(2);
	
	Max->Draw("HIST");
	Nominal->Draw("same HIST");
	Min->Draw("same HIST");

	TLegend legend(0.8,0.7,0.48,0.9);
	
	legend.SetFillColor(0);
	legend.SetFillStyle(0);
	legend.SetBorderSize(0);
	legend.SetTextSize(0.03);
	legend.SetTextAlign(11);
	legend.SetHeader(Form("Channel: %s, Region: %s",channels_str[chan].c_str(), reg_str[reg].c_str()));
	
	legend.AddEntry(Nominal, "Nominal");
	legend.AddEntry(Max, "Up");
	legend.AddEntry(Min, "Down");
	legend.Draw("same");

	
	TPad down(Form("PadDown_%s_Env_Region_%s_Channel_%s_Sample_%s", tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()), "Pad Down", 0, 0.05, 1, 0.3);
	c.cd();
	down.Draw();
	down.cd();
	
	TH1F* h_R_Up = (TH1F*) Max->Clone(); h_R_Up->SetName(Form("%s_Region_%s_Channel_%s_Up", tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str()));
	TH1F* h_R_Down = (TH1F*) Min->Clone(); h_R_Down->SetName(Form("%s_Region_%s_Channel_%s_Down",tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str()));

	if(tag != "PDFUnc") {
	h_R_Up->Divide(Nominal);
	h_R_Down->Divide(Nominal);
	
	h_R_Up->GetYaxis()->SetTitle("Var/Nominal");
	h_R_Up->GetYaxis()->SetRangeUser(0.5, 1.5);
	h_R_Down->GetYaxis()->SetRangeUser(0.5, 1.5);
	h_R_Up->Draw("HIST");
	h_R_Down->Draw("same HIST");
	
	}

	c.SaveAs(Form("SubPlot_%s_UpDown_Region_%s_Channel_%s_Sample_%s.pdf", tag.Data(), reg_str[reg].c_str(), channels_str[chan].c_str(), m_TreeName.Data()));

	PrintSystematic(Nominal, h_R_Up, h_R_Down, chan, reg);
	
	
	h_R_Up->GetYaxis()->SetRangeUser(-1, 1);
	h_R_Down->GetYaxis()->SetRangeUser(-1, 1);
	h_R_Up->GetYaxis()->SetTitle("Sys");
	h_R_Down->GetYaxis()->SetTitle("Sys");
	
	
	if(tag == "ScalUnc") {
	
		h_R_Up->SetDirectory(m_ScaleSys); // save the up and down systematic
		h_R_Down->SetDirectory(m_ScaleSys);
		
	}
	if(tag == "PDFUnc") {
		
		h_R_Up->SetDirectory(m_PDFSys);
		h_R_Down->SetDirectory(m_PDFSys);
	}	
	
	return;
}

void TheorySys::PrintSystematic(TH1F* Nominal, TH1F* Max, TH1F* Min, int chan, int reg) {

	for(int i = 1; i<=Nominal->GetNbinsX(); i++) {
		
		double bin_low  = Nominal->GetXaxis()->GetBinLowEdge(i); 
		double bin_high = Nominal->GetXaxis()->GetBinUpEdge(i);
		
		double sys_up   = (Max->GetBinContent(i) - 1)*100;
		double sys_down = (Min->GetBinContent(i) - 1)*100;
		
		Max->SetBinContent(i, Max->GetBinContent(i) - 1);
		Min->SetBinContent(i, Min->GetBinContent(i) - 1);		
		
		cout<< "Region: "<< reg_str[reg] << " channel: "<< channels_str[chan] << " BDT in ["<< bin_low <<", "<< bin_high<< "] : "<< sys_up << " , " << sys_down <<endl;
		// To be saved in a Text File
	}
	
}

void TheorySys::FindBin() {

	double val = 0.5;
	double max = 1.0;
	double bin = 0.;
	while(val < max)
	{
		double Nevent = 0;
		for(int j = 0; j<m_chain->GetEntriesFast(); j++) {
			m_chain->GetEntry(j);
		
			float lumi = getLumi(m_chain); 
			
			float Total_wgt = (weight*GetBr(runNumber)*lumi);
			if(!isBaseline()) continue;
			if(!isSR(channels::ee)) continue;
			
			if(BDT_output > val) {
	 			Nevent += Total_wgt;	
	 		}	
		}
		if(Nevent < 1.0) break;
		bin = val;
		val = val + 0.01;
		
		
	}
	
	cout<<"The smallest bin on BDT with at leat one event is " << bin<< endl;
}

void TheorySys::SetVarsIndex() {
	
	if(isZgQCD() or isWgQCD()) {
		vars["Nominal"] = 0;
		vars["muF_up"] = 12;
		vars["muF_down"] = 8;
		vars["muR_up"] = 14;
		vars["muR_down"] = 6;
		vars["muF_muR_up"] = 16;
		vars["muF_muR_down"] = 4;
	}
	
	if(isttV()) {
		vars["Nominal"] = 0;
		vars["muF_up"] = 1;
		vars["muF_down"] = 2;
		vars["muR_up"] = 3;
		vars["muR_down"] = 6;
		vars["muF_muR_up"] = 4;
		vars["muF_muR_down"] = 8;
	}
	
	if(runNumber == 410389) {
		vars["Nominal"] = 0;
		vars["muF_up"] = 77;
		vars["muF_down"] = 44;
		vars["muR_up"] = 33;
		vars["muR_down"] = 22;
		vars["muF_muR_up"] = 99;
		vars["muF_muR_down"] = 55;
	
	}
	
	if(isttbar() or istop1()) {
	
		vars["Nominal"] = 0;
		vars["muF_up"] = 1;
		vars["muF_down"] = 2;
		vars["muR_up"] = 3;
		vars["muR_down"] = 4;
		vars["muF_muR_up"] = 6;
		vars["muF_muR_down"] = 5;
	}
	
	if(isttHZy() or isVHZy()) {
	
		vars["Nominal"] = 0;
		vars["muF_up"] = 6;
		vars["muF_down"] = 3;
		vars["muR_up"] = 2;
		vars["muR_down"] = 1;
		vars["muF_muR_up"] = 8;
		vars["muF_muR_down"] = 4;
	}
	
	if(isVHZll() or isHyGr()) {
		
		vars["Nominal"] = 0;
		vars["muF_up"] = 7;
		vars["muF_down"] = 4;
		vars["muR_up"] = 3;
		vars["muR_down"] = 2;
		vars["muF_muR_up"] = 9;
		vars["muF_muR_down"] = 5;
	}
	
	if(isWty() or isWgEWK() or isZgEWK()) {
	
		vars["Nominal"] = 85;  // fix madgraph issues ?
		vars["muF_up"] = 63;
		vars["muF_down"] = 53;
		vars["muR_up"] = 74;
		vars["muR_down"] = 101;
		vars["muF_muR_up"] = 80;
		vars["muF_muR_down"] = 0;
	}
	
}

vector<int> TheorySys::getPDFIndex() {

	vector<int> index; 
	index.push_back(0);

	if(isttV() or isttHZy() or isVHZy()){
		
		for(int i = 9; i<109; i++) {
			index.push_back(i);
		}
		return index;
	}
	
	if(runNumber == 410389) {
		
		for(int i = 2; i<110; i++) { // avoid pdf member 0 --> nominal
		
			if(i == 22 or i == 33 or i == 44 or i == 55 or i == 66 or i == 77 or i == 88 or i == 99) continue;
			
			index.push_back(i);  
			
		}
		return index;
	}
	
	if(isHyGr() or isVHZll()) {
	
		for(int i = 10; i<110; i++) {
			index.push_back(i);
		}
		return index;
	}
	
	if(isttbar()) {
	
		for(int i = 15; i<115; i++) {
			index.push_back(i);
		}
		return index;
	}
	
	if(istop1()) {
		for(int i = 12; i<112; i++) {
			index.push_back(i);
		}
		return index;
	}
	
	if(isVVy() or isVjets()) {
		
		for(int i = 11; i<111; i++) {
			index.push_back(i);
		}
		return index;
	}
	
	if(isWgQCD() or isZgQCD()) {
	
		for(int i = 18; i<218; i=i+2) {
			index.push_back(i);
		}
		return index;
	}

	if(isWgEWK() or isWty() or isZgEWK()) {
		
		for(int i = 2; i<52; i++) {
			if(i == 12 or i == 23 or i == 34 or i == 45 or i == 52) continue;
			index.push_back(i);
		}
		
		for(int i = 86; i<145; i++) { 
			if(i == 90 or i == 101 or i == 112 or i == 123 or i == 134) continue;
			index.push_back(i);
		}
		return index;
	}
	
	cout<<"Sample is not defined" <<endl;
	return index;
}

void TheorySys::GetTree() {

	m_chain = new TChain(m_TreeName);

	for(auto file : m_samples[0])
	{
		m_chain->AddFile(m_path+"/mc16a/"+file,-1,m_TreeName);
		m_chain->AddFile(m_path+"/mc16d/"+file,-1,m_TreeName);
		m_chain->AddFile(m_path+"/mc16e/"+file,-1,m_TreeName);
	}
	
	linkBranches(m_chain);
	
	m_output_Tree = new TFile("Tree_"+m_TreeName+"_output.root", "RECREATE");
	m_Tree = new TTree("Tree", "Tree");
	m_Tree->Branch("weight", &m_weight);
	m_Tree->Branch("isBaseline", &m_isBaseline);
	m_Tree->Branch("isLetPt", &m_isLetPt);
	m_Tree->Branch("isSR", &m_isSR);
	m_Tree->Branch("isAHOI", &m_isAHOI);
	m_Tree->Branch("isEl", &m_isEl);
	m_Tree->Branch("isMu", &m_isMu);
	m_Tree->Branch("mT", &m_mT);
	m_Tree->Branch("BDT_weight_noAHOI", &m_BDT_weight_noAHOI);
	m_Tree->Branch("BDT_weight_wAHOI", &BDT_weight_wAHOI);
	
	int Nevent = m_chain->GetEntriesFast();
	for(int i = 0; i<Nevent; i++) {

		m_chain->GetEntry(i);
		
		if(i%10000 == 0) cout<< " Events "<<i<<"/"<<Nevent<<endl;
		
		TLorentzVector MET;
		MET.SetPtEtaPhiM(met_tight_tst_et/1.0e3, 0, met_tight_tst_phi, 0);
		
		float lumi = getLumi(m_chain); 
		
		float Total_wgt = (weight*GetBr(runNumber)*lumi);
		
		m_weight = Total_wgt;
		m_isBaseline = isBaseline();
		
		m_isSR = PassRegion(0, 2); // both electron and muon
		m_isEl = PassRegion(0, 0);
		m_isMu = PassRegion(0, 1);
		
		m_BDT_weight_noAHOI = BDT_output;
		BDT_weight_wAHOI = BDT_output_AHOI;
		
		TLorentzVector m_Z = RecZboson();
		m_isLetPt = (m_lep1.Pt() > 27 ) && (m_lep2.Pt() > 20);
		
		m_isAHOI = passAHOI(m_Z.M(), m_ph.Pt(), (m_Z+m_ph).M(), met_tight_tst_et*1e-3); 
		
		m_mT = sqrt(2. * m_ph.Pt() * MET.Pt() * (1. - cos(m_ph.Phi() - MET.Phi()))); // in GeV
		
		m_Tree->Fill();	

	}

	m_output_Tree->Write();
	m_output_Tree->Close();
	return;
	

}
